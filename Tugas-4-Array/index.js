// var arr = [1, 2, "three", "four", true, false, [5, 6]];

// console.log(arr);

// var numbers = [0, 1, 2];
// numbers.push(3, 4, 5);

// console.log(numbers);

// var angka = [0, 1, 2, 3];
// var irisan1 = angka.slice(2);

// console.log(irisan1);

// var fruits = ["banana", "orange", "grape"];
// fruits.splice(1, 2, "pineapple", "jackfruits");

// console.log(fruits);

// var arr = [[1, 2, 3, [30, 40, 50]], [4, 5], [6]];

// console.log(arr);

// // console.log(arr[1]);
// // console.log(arr[1][0]);

// // console.log(arr[1][3][1]);
// // console.log(arr[1][3]);
// console.log(arr[1][3]?.[1]);

// var arr = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// arr.forEach(function (value, index) {
// 	console.log("Index ", index, " value ", value);
// });

// console.log(
// 	arr.map(function (value) {
// 		return value / 2;
// 	}),
// );

// console.log(
// 	arr.filter(function (value) {
// 		return value % 3 === 0;
// 	}),
// );

// console.log(
// 	arr.reduce(function (sum, value) {
// 		return value + sum;
// 	}, -50),
// );

// [absesn, nama, nilai]
var dataSiswa = [
	[3, "John", 80],
	[4, "John", 70],
	[2, "John", 90],
	[1, "John", 90],
];

dataSiswa.sort(function (a, b) {
	return b[2] - a[2];
});

console.log(dataSiswa);
