//file: Tugas13b/screens/CounterScreen.js
import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, incrementByAmount } from "../redux/counterSlice";

export default function CounterScreen() {
	const dispatch = useDispatch();

	const count = useSelector((state) => state.counter.value);

	return (
		<View style={{ flex: 1, justifyContent: "center" }}>
			<Button title="Increment" onPress={() => dispatch(increment())} />
			<Text>{count}</Text>
			<Button title="Decrement" onPress={() => dispatch(decrement())} />
			<Button
				title="Increment by 15"
				onPress={() => dispatch(incrementByAmount({ value: 15 }))}
			/>
		</View>
	);
}

const styles = StyleSheet.create({});
