//file: Tugas13b/index.js
import React from "react";

import { store } from "./redux/store";
import { Provider } from "react-redux";

import CounterScreen from "./screens/CounterScreen";

export default function Tugas13() {
	return (
		<Provider store={store}>
			<CounterScreen />
		</Provider>
	);
}
