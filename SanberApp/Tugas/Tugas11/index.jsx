import React, { useEffect, useState } from "react";
import { Button, View, Text, TextInput } from "react-native";

const App = () => {
	// Deklarasi variabel state baru yang kita sebut "count"
	const [count, setCount] = useState(0);
	const [name, setName] = useState("");

	useEffect(() => {
		console.log({ name });
	}, [name]);

	useEffect(() => {
		const timerRef = setInterval(() => {
			setCount((prevCount) => prevCount + 1);
		}, 1000);

		return () => {
			clearInterval(timerRef);
		};
	}, []);

	return (
		<View style={{ justifyContent: "center", flex: 1, paddingHorizontal: 16 }}>
			<Text>Count Increment {count} times</Text>
			<Text>Name {name}</Text>
			<TextInput value={name} onChangeText={setName} />
		</View>
	);
};

export default App;
