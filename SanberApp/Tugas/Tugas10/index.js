import React from "react";
import { Button, StyleSheet, Text, TextInput, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const Flex = () => {
	return (
		<View
			style={{
				flex: 1,
				// borderWidth: 5,
				// borderColor: "red",
				// alignItems: "center",
			}}>
			<View style={styles.browser}>
				<View style={styles.browserIcon}>
					<MaterialCommunityIcons name="instagram" size={28} color="white" />
				</View>
				<View style={styles.browserTextContainer}>
					<Text style={styles.browserTitle}>Instagram follow</Text>
					<Text style={styles.browserSubtitle}>Start your own today !</Text>
				</View>
				<View style={styles.browserNewTag}>
					<Text style={styles.browserNewTagText}>New</Text>
				</View>
			</View>
			{/* <Text style={{ marginBottom: 40, alignSelf: "center" }}>Header</Text>
			<View style={{ flex: 1 }}>
				<TextInput style={{ borderWidth: 1 }} placeholder="Input Your Email" />
				<TextInput
					style={{ borderWidth: 1 }}
					placeholder="Input Your Password"
				/>
			</View>
			<View style={{ borderWidth: 10, borderColor: "blue", height: 400 }}>
				<View style={{ height: "60%", backgroundColor: "red" }} />
				<View style={{ height: "60%", backgroundColor: "yellow" }} />
			</View>
			<Button title="Login" onPress={() => {}} />
			<View style={{ position: "absolute", right: 20, top: 20 }}>
				<Button title="FAB" onPress={() => {}} />
			</View> */}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		backgroundColor: "#4f4f4f",
	},
	browser: {
		borderWidth: 1,
		borderRadius: 10,
		borderColor: "#E8E8E8",
		paddingVertical: 20,
		paddingHorizontal: 15,
		flexDirection: "row",
		overflow: "hidden",
	},
	browserIcon: {
		backgroundColor: "#EC3972",
		borderRadius: 20,
		height: 40,
		width: 40,
		alignItems: "center",
		justifyContent: "center",
		marginRight: 15,
	},
	browserTextContainer: {
		flex: 1,
	},
	browserTitle: {
		fontWeight: "bold",
		fontSize: 16,
		marginBottom: 2,
		color: "#252525",
	},
	browserSubtitle: {
		color: "#888888",
		fontWeight: "700",
		fontSize: 12,
	},
	browserNewTag: {
		position: "absolute",
		transform: [
			{
				rotate: "45deg",
			},
		],
		right: -15,
		top: -10,
		backgroundColor: "#EC3972",
	},
	browserNewTagText: {
		color: "white",
		fontWeight: "700",
		alignItems: "center",
		justifyContent: "center",
		paddingTop: 20,
		paddingBottom: 5,
		paddingHorizontal: 20,
	},
});

export default Flex;
