import { Button, Image, View, Text } from "react-native";
import React from "react";
import { NavigationContainer, useFocusEffect } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

function LogoTitle() {
	return (
		<Image
			style={{ width: 50, height: 50 }}
			source={require("../../assets/favicon.png")}
		/>
	);
}

function HomeScreen({ navigation }) {
	return (
		<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
			<Text>Home Screen</Text>
			<Button
				title="Go to Details"
				onPress={() => {
					/* 1. Navigate to the Details route with params */
					navigation.navigate("Details", {
						itemId: 999,
						otherParam: "anything you want here",
					});
				}}
			/>
			<Button
				title="Update the title"
				onPress={() => navigation.setOptions({ title: "Updated!" })}
			/>
		</View>
	);
}

function DetailsScreen({ route, navigation }) {
	/* 2. Get the param */
	const { itemId, otherParam } = route.params;

	useFocusEffect(
		React.useCallback(() => {
			// Do something when the screen is focused
			console.log("Screen Details is focused");

			return () => {
				// Do something when the screen is unfocused
				// Useful for cleanup functions
				console.log("Screen Details is unfocused");
			};
		}, []),
	);

	return (
		<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
			<Text>Details Screen</Text>
			<Text>itemId: {JSON.stringify(itemId)}</Text>
			<Text>otherParam: {JSON.stringify(otherParam)}</Text>
			<Button
				title="Go to Details... again"
				onPress={() =>
					navigation.push("Details", {
						itemId: Math.floor(Math.random() * 100),
					})
				}
			/>
			<Button title="Go to Home" onPress={() => navigation.navigate("Home")} />
			<Button title="Go back" onPress={() => navigation.goBack()} />
		</View>
	);
}

const Tugas12 = () => {
	return (
		<NavigationContainer>
			<Stack.Navigator
				screenOptions={{
					headerStyle: {
						backgroundColor: "#f4511e",
					},
					headerTintColor: "#fff",
				}}>
				<Stack.Screen
					name="Home"
					component={HomeScreen}
					options={{
						headerTitle: (props) => <LogoTitle {...props} />,
						headerRight: () => (
							<Button
								onPress={() => alert("This is a button!")}
								title="Info"
								color="blue"
							/>
						),
						headerLeft: () => (
							<Button
								onPress={() => alert("This is a button!")}
								title="Info"
								color="blue"
							/>
						),
					}}
				/>
				<Stack.Screen name="Details" component={DetailsScreen} />
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default Tugas12;
