// App.js
import { StyleSheet, Text, View } from "react-native";
import React, { useEffect, useState } from "react";

const App = () => {
	const [counter1, setCounter1] = useState(0);
	const [counter2, setCounter2] = useState(3000);

	useEffect(() => {
		const timer1 = setInterval(() => {
			setCounter1((prevCount) => ++prevCount);
		}, 1000);

		const timer2 = setInterval(() => {
			setCounter2((prevCount) => --prevCount);
		}, 1000);

		return () => {
			clearInterval(timer1);
			clearInterval(timer2);
		};
	}, []);

	return (
		<View style={styles.container}>
			<View style={styles.counter}>
				<Text>Counter 1:</Text>
				<Text>{counter1}</Text>
			</View>
			<View style={styles.counter}>
				<Text>Counter 2</Text>
				<Text>{counter2}</Text>
			</View>
		</View>
	);
};

export default App;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		justifyContent: "center",
	},
	counter: {
		borderWidth: 1,
		borderRadius: 8,
		borderColor: "#eaeaea",
		padding: 10,
		marginVertical: 5,
		flexDirection: "row",
		justifyContent: "space-between",
	},
});
