import {
	Button,
	FlatList,
	KeyboardAvoidingView,
	View,
	Text,
	TextInput,
} from "react-native";
import React, { useEffect, useState } from "react";
import axios from "axios";

const Tugas14 = () => {
	const [title, setTitle] = useState("");
	const [value, setValue] = useState("");

	const [news, setNews] = useState([]);

	const fetchNews = async () => {
		try {
			const newsResult = await axios.get(
				"https://sanbers-news-api.herokuapp.com/api/news",
			);
			const newsData = newsResult.data.results.news;

			console.log({ newsData });

			setNews(newsData);
		} catch (error) {}
	};

	useEffect(() => {
		fetchNews();

		return () => {};
	}, []);

	const onNewsAdded = async () => {
		try {
			const body = {
				title,
				value,
			};

			await axios.post("https://sanbers-news-api.herokuapp.com/api/news", body);

			fetchNews();
			setTitle("");
			setValue("");
		} catch (error) {
			console.error(error.response.data);
		}
	};

	return (
		<KeyboardAvoidingView style={{ padding: 20 }}>
			<Text>Tugas14</Text>
			<FlatList
				data={news}
				renderItem={({ item }) => (
					<View style={{ marginBottom: 10 }}>
						<Text>{item.title}</Text>
						<Text>{item.value}</Text>
					</View>
				)}
				keyExtractor={(item) => item._id}
			/>
			<TextInput
				style={{ padding: 5, borderWidth: 1, marginBottom: 10 }}
				placeholder="News Title"
				value={title}
				onChangeText={setTitle}
			/>
			<TextInput
				style={{ padding: 5, borderWidth: 1, marginBottom: 10 }}
				placeholder="News Value"
				value={value}
				onChangeText={setValue}
			/>
			<Button title="Add News" onPress={onNewsAdded} />
		</KeyboardAvoidingView>
	);
};

export default Tugas14;
