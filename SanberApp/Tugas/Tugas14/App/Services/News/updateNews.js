import { client } from "../client";

export const updateNews = ({ newsId, title, value }) =>
	client.put(`/news/${newsId}`, { title, value });
