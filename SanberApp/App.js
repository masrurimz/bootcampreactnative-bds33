import React from "react";
import {
	StyleSheet,
	Text,
	View,
	SafeAreaView,
	SectionList,
	StatusBar,
} from "react-native";
import { initializeApp, getApps } from "firebase/app";

// import Tugas10 from "./Tugas/Tugas10";
// import Tugas11 from "./Tugas/Tugas11";
// import { ListMovie } from "./Tugas/Tugas11/State";
// import LifeCycle from "./Tugas/Tugas11/LifeCycle";
// import Tugas12 from "./Tugas/Tugas12";
// import Tugas13aContext from "./Tugas/Tugas13a-Context";
// import Tugas13b from "./Tugas/Tugas13b-Redux";
// import Tugas14 from "./Tugas/Tugas14";
import Quiz3 from "./Quiz3";

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyDqWcJ_YfENdlpt9tBZMfkeM_sFjgiBmHw",
	authDomain: "sanberapps-bds33.firebaseapp.com",
	projectId: "sanberapps-bds33",
	storageBucket: "sanberapps-bds33.appspot.com",
	messagingSenderId: "690644785280",
	appId: "1:690644785280:web:f2e073082ee0bf88526e65",
};

// Initialize Firebase
// Doing checking to prevent app crash when hot reloading
if (!getApps().length) {
	initializeApp(firebaseConfig);
}

console.log(getApps());

import Telegram from "./Tugas/Tugas8/Telegram";

// const DATA = [
// 	{
// 		title: "Main dishes",
// 		data: ["Pizza", "Burger", "Risotto"],
// 	},
// 	{
// 		title: "Sides",
// 		data: ["French Fries", "Onion Rings", "Fried Shrimps"],
// 		renderItem: ({ item }) => <Text>{item}</Text>,
// 	},
// 	{
// 		title: "Drinks",
// 		data: ["Water", "Coke", "Beer"],
// 	},
// 	{
// 		title: "Desserts",
// 		data: ["Cheese Cake", "Ice Cream"],
// 	},
// ];

// const Item = ({ title }) => (
// 	<View style={styles.item}>
// 		<Text style={styles.title}>{title}</Text>
// 	</View>
// );

const App = () => (
	<Telegram />
	// <Quiz3 />
	// <Tugas14 />
	// <Tugas13aContext />
	// <LifeCycle />
	// <ListMovie />
	// <SafeAreaView style={styles.container}>
	// <Tugas12 />
	// {/* <Tugas10 /> */}
	// {/* <SectionList
	// 	sections={DATA}
	// 	keyExtractor={(item, index) => item + index}
	// 	renderItem={({ item }) => <Item title={item} />}
	// 	renderSectionHeader={({ section: { title } }) => (
	// 		<Text style={styles.header}>{title}</Text>
	// 	)}
	// /> */}
	// </SafeAreaView>
);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: StatusBar.currentHeight,
		marginHorizontal: 16,
	},
	item: {
		backgroundColor: "#f9c2ff",
		padding: 20,
		marginVertical: 8,
	},
	header: {
		fontSize: 32,
		backgroundColor: "#fff",
	},
	title: {
		fontSize: 24,
	},
});

export default App;
