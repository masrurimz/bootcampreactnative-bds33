// setTimeout(() => {
// 	console.log("Saya menjalankan task A");
// 	setTimeout(() => {
// 		console.log("Saya menjalankan task B");
// 		setTimeout(() => {
// 			console.log("Saya menjalankan task C");
// 			setTimeout(() => {
// 				console.log("Saya menjalankan task E");
// 				setTimeout(() => {
// 					console.log("Saya menjalankan task F");
// 					setTimeout(() => {
// 						console.log("Saya menjalankan task G");
// 					}, 3000);
// 				}, 3000);
// 			}, 3000);
// 		}, 3000);
// 	}, 3000);
// }, 3000);

// console.log("Saya dijalankan duluan");

// const periksaDokter = (nomorAntri, callback) =>
// 	setTimeout(() => {
// 		console.log("Sekarang antriak ke", nomorAntri);
// 		console.log("Isi dari callback", callback);
// 		if (nomorAntri > 50) {
// 			callback(false);
// 		} else {
// 			callback(true);
// 		}
// 	}, 3000);

// periksaDokter(10, (isCalled) => {
// 	if (isCalled) {
// 		console.log("Sebentar lagi giliran saya");
// 	} else {
// 		console.log("Saya akan push rank dulu");
// 	}
// });

// const willIGetNewPhone = (isMomHappy) =>
// 	new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			if (isMomHappy) {
// 				const phone = {
// 					id: 1,
// 					brand: "StangStung",
// 				};

// 				console.log("resolved");
// 				resolve(phone);
// 			} else {
// 				const reason = new Error("Mom is not happy");
// 				reject(reason);
// 			}
// 		}, 3000);
// 	});

const runMe = () => {
	willIGetNewPhone(true)
		.then((phone) => {
			console.log(phone);
			willIGetNewPhone(true)
				.then((phone) => {
					console.log(phone);
					willIGetNewPhone(true)
						.then((phone) => {
							console.log(phone);
							willIGetNewPhone(true)
								.then((phone) => {
									console.log(phone);
								})
								.catch((err) => {
									console.log(err);
								})
								.finally(() => {
									console.log("Saya makan");
								});
						})
						.catch((err) => {
							console.log(err);
						})
						.finally(() => {
							console.log("Saya makan");
						});
				})
				.catch((err) => {
					console.log(err);
				})
				.finally(() => {
					console.log("Saya makan");
				});
		})
		.catch((err) => {
			console.log(err);
		})
		.finally(() => {
			console.log("Saya makan");
		});
};

// runMe();

const runeMeAsync = async () => {
	try {
		let phone;
		phone = await willIGetNewPhone(true);
		console.log(phone);

		phone = await willIGetNewPhone(false);
		console.log(phone);

		phone = await willIGetNewPhone(true);
		console.log(phone);

		phone = await willIGetNewPhone(true);
		console.log(phone);
		// console.log("Saya makan");
	} catch (error) {
		console.log(error);
		// console.log("Saya makan");
	} finally {
		console.log("Saya makan");
	}
};

runeMeAsync();
