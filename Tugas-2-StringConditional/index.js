// var sayHello = "Hello World!";
// console.log(sayHello);

// var name = "John"; // Tipe
// var angka = 12;
// var todayIsFriday = false;

// console.log(name); // "John"
// console.log(angka); // 12
// console.log(todayIsFriday); // false

// var items;
// console.log(items); // Undefined

// var a = 3;
// // var b = 2;

// var hasil = a % 2;
// var isEven = hasil === 0;

// console.log(hasil);
// console.log(isEven);

// var angka = 8;
// console.log(angka + 8);
// console.log(angka + "8");

// console.log(angka == "8"); // true, padahal "8" adalah string.
// console.log(angka === "8"); // false, karena tipe data nya berbeda
// console.log(angka === 8); // true

// var word = "Javascript is awesome";
// console.log(word.length); // 21
// console.log("Last Character", word[word.length - 1]);

// var string1 = "good";
// var string2 = "luck";
// console.log(string1.concat(string2, " at ", " bootcamp ", " bathc ", " 33 ")); // goodluck

// var motor1 = "zelda motorsports";
// var motor2 = motor1.substr(6, 5);
// var motor3 = motor1.substring(6, 11);
// console.log(motor2); // ld
// console.log(motor3); // ld

// var kata1 = "Motor";
// var kata2 = "Sports";

// var kata3 = kata1 + " " + kata2;
// console.log(kata3);

// var mood = "SAD";
// if (mood.toLowerCase() === "happy") {
// 	console.log("hari ini aku bahagia!");
// } else if (mood.toLowerCase() === "sad") {
// 	console.log("hari ini aku sedih!");
// } else {
// 	console.log("hari ini aku absurd");
// }

// var buttonPushed = 100;
// switch (buttonPushed) {
// 	case 1: {
// 		console.log("matikan TV!");
// 		break;
// 	}
// 	case 2: {
// 		console.log("turunkan volume TV!");
// 		break;
// 	}
// 	case 3: {
// 		console.log("tingkatkan volume TV!");
// 		break;
// 	}
// 	case 4: {
// 		console.log("matikan suara TV!");
// 		break;
// 	}
// 	case "lima": {
// 		console.log("ledakkan TV!");
// 		break;
// 	}
// 	default: {
// 		console.log("Tidak terjadi apa-apa");
// 	}
// }

var age = 20;
var votableIf = "";
if (age > 18) {
	votableIf = "Too young";
} else {
	votableIf = "Old enough";
}
console.log(votableIf);

var voteable = age < 18 ? "Too young" : "Old enough";

console.log(voteable);
