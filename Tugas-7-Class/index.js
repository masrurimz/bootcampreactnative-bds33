class Vechile {
	isDiesel = true;

	constructor(brand) {
		// console.log("COnstructor dipanggil");
		// const brand = 10;

		// console.log({ isDiesel });
		// console.log(this.isDiesel);

		this._brand = brand;
		this.wheels = 2;
	}

	set brand(x) {
		this._brand = x[0].toUpperCase() + x.slice(1).toLowerCase();
	}

	get brand() {
		return this._brand.toUpperCase();
	}

	present(greetings) {
		return `${greetings}, I have a ${this._brand}`;
	}

	greetings() {
		return this.present("Holla");
	}

	static hello() {
		return "Hello!!";
	}
}

// const kendaraan = new Vechile("Ford");
// console.log(kendaraan);

// console.log(car.present("Hello"));
// console.log(car.greetings());

// console.log(Vechile.hello());
// console.log(car.hello());

// Inheritance
// class Car extends Vechile {
// 	constructor() {
// 		super("Ferrari");
// 		this.model = "S2000";
// 		this.wheels = 4;
// 		// const car = new Vechile("Ford");
// 	}

// 	show() {
// 		return `${this.present("Hi")}, it is a ${this.model}`;
// 	}
// }

// const mcQueen = new Car();
// console.log(mcQueen);
// console.log(mcQueen.show());

// class MotorCycle extends Vechile {
// 	constructor() {
// 		super("Honda");
// 		this.isDiesel = false;
// 	}
// }

// const vario = new MotorCycle();
// console.log(vario);

// Getter dan Setter
// class Car extends Vechile {
// 	constructor() {
// 		super("Ferrari");
// 		this.model = "S2000";
// 		this.wheels = 4;
// 		// const car = new Vechile("Ford");
// 	}

// 	show() {
// 		return `${this.present("Hi")}, it is a ${this.model}`;
// 	}
// }

// const mcQueen = new Car();
// mcQueen.brand = "feRRari"; //Ferrari
// console.log(mcQueen._brand);
// console.log(mcQueen.brand);

// Tugas 2
function Clock({ template }) {
	var timer;

	function render() {
		var date = new Date();

		var hours = date.getHours();
		if (hours < 10) hours = "0" + hours;

		var mins = date.getMinutes();
		if (mins < 10) mins = "0" + mins;

		var secs = date.getSeconds();
		if (secs < 10) secs = "0" + secs;

		var output = template
			.replace("h", hours)
			.replace("m", mins)
			.replace("s", secs);

		console.log(output);
	}

	this.stop = function () {
		clearInterval(timer);
	};

	this.start = function () {
		render();
		timer = setInterval(render, 1000);
	};
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
